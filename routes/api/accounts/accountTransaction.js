const express = require('express');
const router = express.Router();
const passport = require('passport');

//load account transation
const AccountTransaction = require('./../../../models/account/AccountTransaction');

/**
 * @route GET api/account/accountTransactions
 * @desc fetch all account transactions
 * @access private
 */
router.get('/', passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    const errors = {};

    AccountTransaction.find({})
        .then(trans => {
            if (!trans) {
                errors.noresult = "No Records found";
                return res.status(404).json(errors);
            }

            res.json(trans);
        })
        .catch(err => res.status(404).json(err));
});

/**
 * @route GET api/account/accountTransactions/location_ID
 * @desc fetch account transactions by location_ID
 * @access private
 */
router.get('/location/:location', passport.authenticate('jwt', {session: false}), (req, res) => {
    const errors = {};

    console.log(req.query);

    AccountTransaction.find({location: req.params.location})
        .then(transactions => {
            if(!transactions) {
                errors.noresult = "no results found in this location";
                res.status(404).json(errors);
            }

            res.json(transactions);
        })
})

/**
 * @route api/account/accountTransactions
 * @desc get transaction by id
 * @access private
 */
router.get('/:id',passport.authenticate('jwt', {
                session: false
            }), (req, res) => {
    const errors = {};

    AccountTransaction.findOne({_id: req.params.id})
        .findOne(transaction => {
            if(!transaction){
                errors.noresult = 'Record not Found';
                return res.status(404).json(errors);
            }

            res.json(transaction);
        })
        .catch(err => res.status(404).json(err));
})

/**
 * @route POST api/account/accountTransactions
 * @desc save a transaction
 * @access private
 */
router.post('/:id?', passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    const errors = {};

    const inputFields = {};
    const body = req.body;
    const transactionId = req.params.id;

    // res.json(typeof req.body);
    // console.log(req.body.strArr);
    
    inputFields.added_by = req.user.id;
    if (body.transId) inputFields.transId = body.transId;
    if (body.ttype) inputFields.ttype = body.ttype;
    if (body.transdate) inputFields.transdate = body.transdate;
    if (body.reference) inputFields.reference = body.reference;
    if (body.numItems) inputFields.numItems = body.numItems;
    if (body.amount) inputFields.amount = body.amount;
    if (body.location) inputFields.location = body.location;

    if (transactionId) {
        AccountTransaction.findOneAndUpdate({
                _id: transactionId
            }, {
                $set: inputFields
            }, {
                new: true
            })
            .then(transaction => {
                res.json(transaction);
            })
            .catch(err => res.status(404).json(err));
    } else {
        
        new AccountTransaction(inputFields)
            .save()
            .then(transaction => res.json(transaction)).catch(err => res.status(404).json(err));
       
    }
})

module.exports = router;