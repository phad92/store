const express = require("express");
const router = express.Router();
const passport = require("passport");

//Load account voucher
const AccountVoucher = require("../../../models/account/AccountVoucher");

router.get('/', passport.authenticate('jwt', { session: false }), ( req, res) => {
    const errors = {};

    AccountVoucher.find({})
        .then(resp => {
            if(!resp){
                errors.noresults = " No results found";
                res.status(404).json(errors);
            }

            res.json(resp);
        })
        .catch(err => res.status(404).json(err));
});

/**
 * @route POST api/account/voucher/(id)
 * @desc creates or update account voucher(voucher)
 * @access private
 */
router.post(
  "/:id?",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const errors = {};

    const inputFields = {};
    const voucherId = req.params.id;

    if (req.body.name) inputFields.name = req.body.name;
    if (req.body.description) inputFields.description = req.body.description;

    if (voucherId) {
      AccountVoucher.findOneAndUpdate(
        { _id: voucherId },
        { $set: inputFields },
        { new: true }
      )
        .then(resp => res.json(resp))
        .catch(err => res.status(404).json(err));
    } else {
      AccountVoucher.findOne({ name: inputFields.name })
        .then(resp => {
          if (resp) {
            errors.resultexists = resp.name + " Already exists";
            return res.status(404).json(errors);
          }

          new AccountVoucher(inputFields)
            .save()
            .then(resp => res.json(resp))
            .catch(err => res.status(404).json(err));
        })
        .catch(err => res.status(404).json(err));
    }
  }
);

/**
 * @route DELETE api/vouchers/id
 * @desc delete a voucher
 * @access private
 */
router.delete('/:id', passport.authenticate('jwt', {session: false}), (req, res) => {
  AccountVoucher.findOneAndRemove({_id: req.params.id})
    .then(() => res.json({success: true}))
    .catch(err => res.status(404).json(err));
})


module.exports = router;