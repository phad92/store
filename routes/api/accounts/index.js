const express = require('express');
const router = express.Router();

//Stock related route files
const ledgers = require('./accountLedger');
const vouchers = require('./accountVoucher');
const groups = require('./accountGroup');

const accountTransaction = require('./accountTransaction');


//account related routes
router.use('/accountTransactions', accountTransaction);
router.use('/ledgers', ledgers);
router.use('/groups', groups);
router.use('/vouchers', vouchers);

module.exports = router;