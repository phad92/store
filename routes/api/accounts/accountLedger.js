const express = require("express");
const router = express.Router();
const passport = require("passport");

//import account Model
const AccountLedger = require("../../../models/account/AccountLedger");

/**
 * @route GET api/accounts/ledgers
 * @desc fetch account ledgers
 * @access private
 */
router.get(
  "/",
  passport.authenticate("jwt", {
    session: false
  }),
  (req, res) => {
    const errors = {};

    AccountLedger.find({})
      .then(accountLedgers => {
        if (!accountLedgers) {
          errors.noresult = "No record found";
          return res.status(404).json(errors);
        }
        res.json(accountLedgers);
      })
      .catch(err => res.json(err));
  }
);

/**
 * @route POST api/accounts/ledger
 * @desc create/update account ledger
 * @access private
 */
router.post(
  "/:id?",
  passport.authenticate("jwt", {
    session: false
  }),
  (req, res) => {
    const errors = {};

    const inputFields = {};
    const ledgerId = req.params.id;
    const body = req.body;

    inputFields.added_by = req.user.id;
    if (body.ledgerName) inputFields.ledgerName = body.ledgerName;
    if (body.phone) inputFields.phone = body.phone;
    if (body.email) inputFields.email = body.email;
    if (body.address) inputFields.address = body.address;
    if (body.group) inputFields.group = body.group;
    if (body.voucher) inputFields.voucher = body.voucher;

    inputFields.contactPerson = {};
    if (body.contactName) inputFields.contactPerson.contactName = body.contactName;
    if (body.contactPhone) inputFields.contactPerson.contactPhone = body.contactPhone;
    if (body.ContactAddress) inputFields.contactPerson.ContactAddress = body.ContactAddress;

    if (!ledgerId) {
      AccountLedger.findOne({ ledgerName: inputFields.ledgerName })
        .then(ledger => {
          if (ledger) {
            errors.resultexits = inputFields.ledgerName + " already exists";
            return res.status(404).json(errors);
          }

          new AccountLedger(inputFields)
            .save()
            .then(ledger => res.json(ledger))
            .catch(err => res.json(err));
        })
        .catch(err => res.json(err));
    } else {
      AccountLedger.findOneAndUpdate(
        { _id: ledgerId },
        { $set: inputFields },
        { new: true }
      )
        .then(ledger => {
          return res.json(ledger);
        })
        .catch(err => res.status(404).json(err));
    }
  }
);

module.exports = router;
