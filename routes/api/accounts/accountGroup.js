const express = require("express");
const router = express.Router();
const passport = require("passport");

//Load account group
const AccountGroup = require("../../../models/account/AccountGroup");

router.get('/', passport.authenticate('jwt', {
  session: false
}), (req, res) => {
  const errors = {};

  AccountGroup.find({})
    .then(resp => {
      if (!resp) {
        errors.noresults = " No results found";
        res.status(404).json(errors);
      }

      res.json(resp);
    })
    .catch(err => res.status(404).json(err));
});

/**
 * @route POST api/account/group/(id)
 * @desc creates or update account group(group)
 * @access private
 */
router.post(
  "/:id?",
  passport.authenticate("jwt", {
    session: false
  }),
  (req, res) => {
    const errors = {};

    const inputFields = {};
    const groupId = req.params.id;

    if (req.body.name) inputFields.name = req.body.name;
    if (req.body.description) inputFields.description = req.body.description;

    if (groupId) {
      AccountGroup.findOneAndUpdate({
          _id: groupId
        }, {
          $set: inputFields
        }, {
          new: true
        })
        .then(resp => res.json(resp))
        .catch(err => res.status(404).json(err));
    } else {
      AccountGroup.findOne({
          name: inputFields.name
        })
        .then(resp => {
          if (resp) {
            errors.resultexists = resp.name + " Already exists";
            return res.status(404).json(errors);
          }

          new AccountGroup(inputFields)
            .save()
            .then(resp => res.json(resp))
            .catch(err => res.status(404).json(err));
        })
        .catch(err => res.status(404).json(err));
    }
  }
);

/**
 * @route DELETE api/groups/id
 * @desc delete a group
 * @access private
 */
router.delete('/:id', passport.authenticate('jwt', {
  session: false
}), (req, res) => {
  AccountGroup.findOneAndRemove({
      _id: req.params.id
    })
    .then(() => res.json({
      success: true
    }))
    .catch(err => res.status(404).json(err));
})


module.exports = router;