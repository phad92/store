const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const passport = require("passport");

//models
const Profile = require("../../../models/Profile");
const User = require("../../../models/User");

//import validation file
const validateProfileInput = require("../../../validation/profile");

/**
 * @route Get api/profile
 * @desc Get Current User's Profile
 * @access Private
 */
router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const errors = {};
    Profile.findOne({ user: req.user.id })
      .then(profile => {
        if (!profile) {
          errors.noprofile = "There's no profile for this user";
          return res.status(404).json(errors);
        }

        res.json(profile);
      })
      .catch(err => res.status(404).json(err));
  }
);

/**
 * @route POST api/profile
 * @desc CREATE User's Profile
 * @access Private
 */
router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateProfileInput(req.body);

    //check validation
    if (!isValid) {
      return res.status(400).json(errors);
    }

    //GET FIELDS
    const profileFields = {};
    const body = req.body;

    profileFields.user = req.user.id;

    if (body.avatar) profileFields.avatar = body.avatar;
    if (body.firstname) profileFields.firstname = body.firstname;
    if (body.lastname) profileFields.lastname = body.lastname;
    if (body.gender) profileFields.gender = body.gender;
    if (body.dob) profileFields.dob = body.dob;

    if(typeof body.phone !== 'undefined'){
      profileFields.phone = body.phone.split(',');
    }

    if (body.phone) profileFields.phone = body.phone;
    if (body.email) profileFields.email = body.email;
    if (body.company) profileFields.company = body.company;

    Profile.findOne({ user: req.user.id }).then(profile => {
      if (profile) {
        //update
        Profile.findOneAndUpdate(
          { user: req.user.id },
          { $set: profileFields },
          { new: true }
        ).then(profile => res.json(profile));
      } else {
        // create
        //check if handle exists
        Profile.findOne({ email: profileFields.email }).then(profile => {
          if (profile) {
            errors.emailexists = "Email already exists";
            res.status(404).json(errors);
          }

          //Save Profile
          new Profile(profileFields).save().then(profile => res.json(profile));
        });
      }
    });
  }
);

module.exports = router;
