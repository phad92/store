const express = require('express');
const router = express.Router();

//Stock related route files
const user = require('./user');
const profile = require('./profile');


//stock related routes
router.use('/', user);
router.use('/profile', profile);

module.exports = router;