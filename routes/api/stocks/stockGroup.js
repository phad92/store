const express = require("express");
const router = express.Router();
const passport = require("passport");

//import Stocks model
const StockGroup = require("../../../models/stock/StockGroup");

/**
 * @route GET /stockGroup
 * @desc fetch stock groups
 * @access private
 */

router.get(
  "/",
  // passport.authenticate("jwt", { session: false }),
  (req, res) => {
    errors = {};
    StockGroup.find({})
      .then(stockGroups => {
        if (!stockGroups) {
          errors.nostockgroups = "No Stock Groups found";
          return res.status(404).json(errors);
        }

        res.json(stockGroups);
      })
      .catch(err => res.status(404).json(err));
  }
);

/**
 * @route POST /stockGroup
 * @desc CREATE OR UPDATE stock groups
 * @access private
 */

router.post(
  "/:id?",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const errors = {};
    const stockGroupId = req.params.id;
    const body = req.body;
    
    const stockGroupInput = {};
    
    if (body.name) stockGroupInput.name = body.name;
    if (body.parent) stockGroupInput.parent = body.parent;
    if (body.description) stockGroupInput.description = body.description;

    if (stockGroupId) {
      StockGroup.findOneAndUpdate(
        { _id: stockGroupId },
        { $set: stockGroupInput },
        { new: true }
      )
        .then(stockGroup => {
          return res.json(stockGroup);
        })
        .catch(err => res.status(404).json(err));
    } else {
      StockGroup.findOne({ name: stockGroupInput.name }).then(stockGroup => {
        if (stockGroup) {
          errors.groupexits = "Group Name already Exists";
          return res.status(404).json(errors);
        }

        new StockGroup(stockGroupInput).save().then(stockGroup => {
          res.json(stockGroup);
        });
      });
    }
  }
);

/**
 * @route DELETE /stockGroup
 * @desc DELETE stock groups
 * @access private
 */
router.delete(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const errors = {};

    const stockGroupId = req.params.id;

    StockGroup.findOneAndRemove({ _id: stockGroupId })
      .then(() => {
        res.json({ success: true });
      })
      .catch(err => res.status(404).json(err));
  }
);

module.exports = router;
