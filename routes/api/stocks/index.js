const express = require('express');
const router = express.Router();

//Stock related route files
const stock = require('./stock');
const stockGroup = require('./stockGroup');
const location = require('./location');


//stock related routes
router.use('/',stock);
router.use('/stockGroup',stockGroup);
router.use('/location',location);

module.exports = router;