const express = require('express');
const router = express.Router();
const passport = require('passport');

//import Stocks model
const Location = require('../../../models/Location');

//import input validation file
const validateWarehouseInput = require("../../../validation/warehouse"); 


/**
 * @route GET /location
 * @desc fetch stock units
 * @access private
 */

router.get('/', passport.authenticate('jwt', { session: false }), (req, res) => {
    errors = {};
    Location.find({})
        .then(locations => {
            if (!locations) {
                errors.nostockunits = 'No Location found';
                return res.status(404).json(errors);
            }

            res.json(locations);
        })
        .catch(err => res.status(404).json(err));
})

/**
* @route POST /location
* @desc CREATE OR UPDATE stock units
* @access private
*/

router.post('/:id?', passport.authenticate('jwt', { session: false }), (req, res) => {
    const {errors, isValid} = validateWarehouseInput(req.body);

    if(!isValid){
        return res.status(404).json(errors);
    }

    const locationId = req.params.id;
    const body = req.body;
    const locationInput = {};
    

    if(body.name) locationInput.name = body.name;

    if(typeof body.phone !== 'undefined') {
        locationInput.phone = body.phone.split(',');
    }
    
    if(body.email) locationInput.email = body.email;
    if(body.address) locationInput.address = body.address;
    if(body.isWarehouse) locationInput.isWarehouse = body.isWarehouse;

    if (locationId) {
        Location.findOneAndUpdate({ _id: locationId }, { $set: locationInput }, { new: true })
            .then(location => {
                return res.json(location);
            })
            .catch(err => res.status(404).json(err))
    } else {

        Location.findOne({ name: locationInput.name })
            .then(location => {
                if (location) {
                    errors.dataexists = "Location Name already Exists";
                    return res.status(404).json(errors);
                }

                new Location(locationInput).save().then(location => {
                    res.json(location);
                })
            })
    }
})

/**
* @route DELETE /location
* @desc DELETE stock units
* @access private
*/
router.delete('/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    const errors = {};

    const locationId = req.params.id;

    Location.findOneAndRemove({ _id: locationId })
        .then(() => {
            res.json({ success: true });
        })
})

// todos
// removing phone numbers

module.exports = router;