const express = require("express");
const router = express.Router();
const passport = require("passport");

//import Stocks model
const Stock = require("../../../models/stock/Stock");

/**
 * THINGS TO ADD
 * add minimun stocklevel field
 */

/**
 * @route GET /stock
 * @desc fetch stock groups
 * @access private
 */

router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    errors = {};
    Stock.find({})
      .then(stocks => {
        if (!stocks) {
          errors.noresult = "No Stocks found";
          return res.status(404).json(errors);
        }

        res.json(stocks);
      })
      .catch(err => res.status(404).json(err));
  }
);

/**
 * @route POST /stock
 * @desc CREATE OR UPDATE stock stocks
 * @access private
 */

router.post(
  "/:id?",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const errors = {};
    const stockId = req.params.id;
    const body = req.body;
    const stockInput = {};

    if (body.name) stockInput.name = body.name;
    if (body.stockGroup) stockInput.stockGroup = body.stockGroup;
   
    stockInput.stockUnit = {};
    if (body.unit) stockInput.stockUnit.unit = body.unit;
    if (body.symbol) stockInput.stockUnit.symbol = body.symbol;
    if (body.description) stockInput.stockUnit.description = body.description;
    
    if (body.price) stockInput.price = body.price;

    if (typeof body.locations !== "undefined") {
      stockInput.locations = body.locations.split(",");
    }

    if (body.remarks) stockInput.remarks = body.remarks;

    if (stockId) {
      Stock.findOneAndUpdate(
        { _id: stockId },
        { $set: stockInput },
        { new: true }
      )
        .then(stock => {
          return res.json(stock);
        })
        .catch(err => res.status(404).json(err));
    } else {
      Stock.findOne({ name: stockInput.name }).then(stock => {
        if (stock) {
          errors.stockexits = stockInput.name + " already Exists";
          return res.status(404).json(errors);
        }

        new Stock(stockInput).save().then(stock => {
          res.json(stock);
        });
      });
    }
  }
);

/**
 * @route DELETE /stock
 * @desc DELETE stock stocks
 * @access private
 */
router.delete(
  "/stock/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const errors = {};

    const stockId = req.params.id;

    Stock.findOneAndRemove({ _id: stockId }).then(() => {
      res.json({ success: true });
    });
  }
);

module.exports = router;
