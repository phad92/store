const express = require('express');
const router = express.Router();
const passport = require('passport');

//import Stocks model
const Warehouse = require('../../../models/stock/Warehouse');

//import input validation file
const validateProfileInput = require("../../../validation/warehouse"); 


/**
 * @route GET /warehouse
 * @desc fetch stock units
 * @access private
 */

router.get('/', passport.authenticate('jwt', { session: false }), (req, res) => {
    errors = {};
    Warehouse.find({})
        .then(warehouses => {
            if (!warehouses) {
                errors.nostockunits = 'No Warehouse found';
                return res.status(404).json(errors);
            }

            res.json(warehouses);
        })
        .catch(err => res.status(404).json(err));
})

/**
* @route POST /warehouse
* @desc CREATE OR UPDATE stock units
* @access private
*/

router.post('/:id?', passport.authenticate('jwt', { session: false }), (req, res) => {
    const {errors, isValid} = validateProfileInput(req.body);

    if(!isValid){
        return res.status(404).json(errors);
    }

    const warehouseId = req.params.id;
    const body = req.body;
    const warehouseInput = {};
    

    if(body.name) warehouseInput.name = body.name;

    if(typeof body.phone !== 'undefined') {
        warehouseInput.phone = body.phone.split(',');
    }
    
    if(body.email) warehouseInput.email = body.email;
    if(body.address) warehouseInput.address = body.address;

    if (warehouseId) {
        Warehouse.findOneAndUpdate({ _id: warehouseId }, { $set: warehouseInput }, { new: true })
            .then(warehouse => {
                return res.json(warehouse);
            })
            .catch(err => res.status(404).json(err))
    } else {

        Warehouse.findOne({ name: warehouseInput.name })
            .then(warehouse => {
                if (warehouse) {
                    errors.unitexits = "Unit Name already Exists";
                    return res.status(404).json(errors);
                }

                new Warehouse(warehouseInput).save().then(warehouse => {
                    res.json(warehouse);
                })
            })
    }
})

/**
* @route DELETE /warehouse
* @desc DELETE stock units
* @access private
*/
router.delete('/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    const errors = {};

    const warehouseId = req.params.id;

    Warehouse.findOneAndRemove({ _id: warehouseId })
        .then(() => {
            res.json({ success: true });
        })
})

// todos
// removing phone numbers

module.exports = router;