const express = require("express");
const router = express.Router();
const passport = require("passport");

//include company in user table.
//load company model
const Company = require("../../models/Company");

//load validation file
const validateCompanyInput = require("../../validation/company");

/**
 * @route GET /api/company
 * @desc fetch companies
 * @access private
 */

router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const errors = {};

    Company.find({})
      .then(companies => {
        if (!companies) {
          errors.notfound = "no result found for companies";
          return res.status(404).json(errors);
        }
        res.json(response);
      })
      .catch(err => res.status(404).json(err));
  }
);

/**
 * @route GET /api/company/:id
 * @desc fetch company
 * @access private
 */
router.get(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const errors = {};
    const companyId = req.params.id;

    Company.findById({ _id: companyId }).then(company => {
      res.json(company);
    });
  }
);

/**
 * @route POST /api/company
 * @desc insert company info
 * @access private
 */

router.post(
  "/:id?",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateCompanyInput(req.body);
    if (!isValid) {
      return res.status(404).json(errors);
    }
    const body = req.body;
    const companyId = req.params.id;
    const companyInputs = {};

    if (body.name) companyInputs.name = body.name;
    if (typeof body.phone !== "undefined") {
      companyInputs.phone = body.phone.split(",");
    }
    if (body.email) companyInputs.email = body.email;
    if (body.address) companyInputs.address = body.address;

    //check if :id is passed in uri
    if (companyId) {
      Company.findByIdAndUpdate(
        { _id: companyId },
        { $set: companyInputs },
        { new: true }
      )
      .then(company => {
          res.json(company);
        })
        .catch(err => res.status(404).json(err));
    } else {
      //check if company exists
      Company.findOne({ name: companyInputs.name }).then(company => {
        if (company) {
          errors.companyexists = "Company name alread exists";
          return res.status(404).json(errors);
        }

        //save inputs
        new Company(companyInputs)
          .save()
          .then(company => res.json(company))
          .catch(err => res.status(404).json(err));
      });
    }
  }
);

/**
 * @route DELETE /api/company/:id
 * @desc Update company info
 * @access private
 */
router.delete('/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    const companyId = req.params.id;

    Company.findOneAndRemove({ _id: companyId })
        .then(() => {
            res.json({ success: true });
        })
});


module.exports = router;
