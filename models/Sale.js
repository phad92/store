const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Create Schema
const SaleSchema = new Schema({
  item_id: {
    type: String,
    required: true
  },
  description: {
    type: String
  }
});

module.exports = Sale = mongoose.model("sales", SaleSchema);
