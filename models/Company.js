const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CompanySchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String
  },
  phone: {
    type: [String],
    required: true
  },
  address: {
    type: String
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Company = mongoose.model("company", CompanySchema);
