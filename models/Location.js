const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const LocationSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    phone: [{
        type: String,
        required: true
    }],
    email: {
        type: String
    },
    address: {
        type: String
    },
    isWarehouse: {
        type: Boolean
    },
    date: {
        type: Date,
        default: Date.now
    }
});

module.exports = Location = mongoose.model('locations', LocationSchema);