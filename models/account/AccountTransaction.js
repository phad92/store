const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Create Schema
const AccountTransactionSchema = new Schema({
    transId: {
        type: String,
        required: true
    },
    ttype: {
        type: String,
        required: true
    },
    transdate: {
        type: Date
    },
    reference: {
        type: String
    },
    numItems: {
        type: Number
    },
    amount: {
        type: mongoose.Decimal128,
        default: 0.00
    },
    location: {
        type: Schema.Types.ObjectId,
        ref: 'location'
    },
    added_by: {
        type: String
    },
    updated_by: {
        type: String
    }
}, {
    timestamps: true
});

module.exports = AccountTransaction = mongoose.model("accountTransactions", AccountTransactionSchema);