const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Create Schema
const AccountLedgerSchema = new Schema({
  ledgerName: {
    type: String,
    required: true
  },
  phone: {
    type: [String]
  },
  email: {
    type: String
  },
  address: {
    type: String
  },
  group: { //Account Group
    type: String,
    required: true
  },
  voucher: { //Account Class
    type: String,
    requird: true 
  },
  contactPerson: [{
    contactName: {
      type: String
    },
    contactPhone: {
      type: String
    },
    contactAddress: {
      type: String
    }
  }],
  added_by: {
    type: Schema.Types.ObjectId,
    ref: "users"
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = AccountLedger = mongoose.model(
  "accountLedger",
  AccountLedgerSchema
);
