const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Create Schema
const AccountClassSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String
  }
});

module.exports = AccountClass = mongoose.model(
  "accountClass",
  AccountClassSchema
);
