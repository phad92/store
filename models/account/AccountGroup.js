const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Create Schema
const AccountGroupSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String
  }
});

module.exports = AccountGroup = mongoose.model("accountGroups", AccountGroupSchema);