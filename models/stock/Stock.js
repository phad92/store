const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Create Schema
const StockSchema = new Schema({
  name: {
    type: String,
    require: true
  },
  stockGroup: {
    type: Schema.Types.ObjectId,
    ref: "stockGroups"
  },
  stockUnit: {
    unit: {
      type: String,
      required: true
    },
    symbol: {
      type: String,
      max: 3
    },
    description: {
      type: String
    }
  },
  cost: {
    type: mongoose.Decimal128
  },
  price: {
    type: mongoose.Decimal128,
    required: true
  },
  locations: [
      { 
        location: {
          type: Schema.Types.ObjectId, 
          ref: "locations" 
        },
        quantity: {
          type: Number,
          default: 0
        },
        minLevel: {
          type: Number,
          default: 0
        }
      }
    ],
  remarks: {
    type: String
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Stock = mongoose.model("stocks", StockSchema);
