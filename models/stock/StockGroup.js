const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Create Schema
const StockGroupSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  parent: {
    type: String
  },
  description: {
    type: String
  }
});

module.exports = StockGroup = mongoose.model("stockGroups", StockGroupSchema);
