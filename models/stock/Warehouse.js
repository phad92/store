const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Create Schema
const WarehouseSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  phone: [
    {
      type: [String],
      required: true
    }
  ],
  email: {
    type: String
  },
  address: {
    type: String
  },
  
});

module.exports = Warehouse = mongoose.model("warehouses", WarehouseSchema);
