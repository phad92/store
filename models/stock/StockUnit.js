const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Create Schema
const StockUnitSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  symbol: {
    type: String
  },
  description: {
    type: String
  }
});

module.exports = StockUnit = mongoose.model("stockUnits", StockUnitSchema);
