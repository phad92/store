const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const StockTransactionSchema = new Schema({
    transDate: {
        type: Date,
        required: true
    },
    rate: {
        type: Number,
        required: true
    },
    amount: {
        type: Number,
    },
    location:{
        type: Schema.Types.ObjectId,
        ref: 'locations'
    },
    inwards: {
        type: Schema.Types.ObjectId,
        ref: 'returnInwards'
    },
    outwards: {
        type: Schema.Types.ObjectId,
        ref: 'returnOutwards'
    },
    date: {
        type: Date,
        default: Date.now
    }
});

module.exports = StockTransaction = mongoose.model('stockTransactions', StockTransactionSchema);