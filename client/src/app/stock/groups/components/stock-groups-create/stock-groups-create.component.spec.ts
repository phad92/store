import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockGroupsCreateComponent } from './stock-groups-create.component';

describe('StockGroupsCreateComponent', () => {
  let component: StockGroupsCreateComponent;
  let fixture: ComponentFixture<StockGroupsCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockGroupsCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockGroupsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
