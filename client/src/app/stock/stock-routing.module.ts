import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StockBaseComponent } from './stock-base/stock-base.component';
import { StockGroupsCreateComponent } from './groups/components/stock-groups-create/stock-groups-create.component';
import { StockGroupsListComponent } from './groups/components/stock-groups-list/stock-groups-list.component';

const StockRoutes: Routes = [
  {
    path: 'stock',
    component: StockBaseComponent,
    children: [
      {
        path: 'groups',
        children: [
          {
            path: '',
            component: StockGroupsListComponent
          }
        ]
      },
      {
        path: 'create-group',
        component: StockGroupsCreateComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(StockRoutes)],
  exports: [RouterModule]
})
export class StockRoutingModule { }
