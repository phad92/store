import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { StockRoutingModule } from './stock-routing.module';
import { StockUnitsListComponent } from './units/components/stock-units-list/stock-units-list.component';
import { StockUnitsCreateComponent } from './units/components/stock-units-create/stock-units-create.component';
import { StockGroupsCreateComponent } from './groups/components/stock-groups-create/stock-groups-create.component';
import { StockGroupsListComponent } from './groups/components/stock-groups-list/stock-groups-list.component';
import { StockBaseComponent } from './stock-base/stock-base.component';

@NgModule({
  declarations: [
    StockUnitsListComponent, 
    StockUnitsCreateComponent, 
    StockGroupsCreateComponent, 
    StockGroupsListComponent, 
    StockBaseComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    StockRoutingModule
  ]
})
export class StockModule { }
