import { TestBed } from '@angular/core/testing';

import { StockUnitService } from './stock-unit.service';

describe('StockUnitService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StockUnitService = TestBed.get(StockUnitService);
    expect(service).toBeTruthy();
  });
});
