import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockBaseComponent } from './stock-base.component';

describe('StockBaseComponent', () => {
  let component: StockBaseComponent;
  let fixture: ComponentFixture<StockBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
