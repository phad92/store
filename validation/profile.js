const isEmpty = require("./is-empty");
const Validator = require("validator");

module.exports = function validateProfileInput(data) {
  let errors = {};

  data.firstname = !isEmpty(data.firstname) ? data.firstname : "";
  data.lastname = !isEmpty(data.lastname) ? data.lastname : "";
  data.company = !isEmpty(data.company) ? data.company : "";
  data.phone = !isEmpty(data.phone) ? data.phone : "";


  if (Validator.isEmpty(data.firstname)) {
    errors.firstname = "First Name field is required";
  }

  if (Validator.isEmpty(data.lastname)) {
    errors.lastname = "Last Name field is required";
  }

  if (Validator.isEmpty(data.company)) {
    errors.company = "Company field is required";
  }


  if (Validator.isEmpty(data.phone)) {
    errors.phone = "Phone field is required";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
