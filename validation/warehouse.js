const isEmpty = require("./is-empty");
const Validator = require("validator");

module.exports = function validateWarehouseInput(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name : "";
  data.email = !isEmpty(data.email) ? data.email : "";
  data.phone = !isEmpty(data.phone) ? data.phone : "";

  if (Validator.isEmpty(data.name)) {
    errors.name = "Name field is required";
  }

  if (!Validator.isLength(data.name, { min: 6, max: 30 })) {
    errors.name = "Password must be atleast 6 characters";
  }

  if (Validator.isEmpty(data.phone)) {
    errors.phone = "Name field is required";
  }

//   if (!Validator.isLength(data.phone, { max: 10 })) {
//     errors.phone = "Password must be 10 characters";
//   }

  if (!Validator.isEmail(data.email)) {
    errors.email = "Email is invalid";
  }

  if (Validator.isEmpty(data.email)) {
    errors.email = "Email field is required";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
