const isEmpty = require('./is-empty');
const Validator =  require('validator');

module.exports = function validateCompanyInput(data){
    let errors = {};

    data.name = !isEmpty(data.name) ? data.name : '';
    data.phone = !isEmpty(data.phone) ? data.phone : '';
    data.email = !isEmpty(data.email) ? data.email : '';
    data.address = !isEmpty(data.address) ? data.address : '';
    
    if(Validator.isEmpty(data.name)){
        errors.name = 'Name field is required';
    }
    
    if (!Validator.isLength(data.name, { min: 2, max: 30 })) {
        errors.name = 'Password must be atleast 2 characters';
    }
    
    if(!Validator.isEmail(data.email)){
        errors.email = 'Email is invalid';
    }
    
    if(Validator.isEmpty(data.email)){
        errors.email = 'Email field is required';
    }
    
    if(Validator.isEmpty(data.phone)){
        errors.phone = 'Phone Number field is required';
    }
    

    return {
        errors,
        isValid: isEmpty(errors)
    }
}