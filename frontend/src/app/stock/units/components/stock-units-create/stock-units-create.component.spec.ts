import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockUnitsCreateComponent } from './stock-units-create.component';

describe('StockUnitsCreateComponent', () => {
  let component: StockUnitsCreateComponent;
  let fixture: ComponentFixture<StockUnitsCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockUnitsCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockUnitsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
