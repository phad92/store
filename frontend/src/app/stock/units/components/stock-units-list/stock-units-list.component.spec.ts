import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockUnitsListComponent } from './stock-units-list.component';

describe('StockUnitsListComponent', () => {
  let component: StockUnitsListComponent;
  let fixture: ComponentFixture<StockUnitsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockUnitsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockUnitsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
