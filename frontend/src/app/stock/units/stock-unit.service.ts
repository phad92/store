import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_URL } from 'src/app/config/constants';

export interface stockUnitModel{
  name: string,
  symbol: string,
  description: string
}

@Injectable({
  providedIn: 'root'
})
export class StockUnitService {

  constructor(private _http: HttpClient) { }

  getStockUnits(){
    return this._http.get(API_URL + '/stock/stockUnit');
  }

  getStockUnit(id){
    return this._http.get(API_URL + '/stock/stockUnit/' + id);
  }

  addStockUnitS(data){
    return this._http.post(API_URL + '/stock/stockUnit', data);
  }



}