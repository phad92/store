import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_URL } from 'src/app/config/constants';

export interface stockGroupsModel {
  name: string,
  symbol: string,
  description: string
}
@Injectable({
  providedIn: 'root'
})
export class StockGroupsService {

  constructor(private _http: HttpClient) { }

  getStockGroups() {
    return this._http.get(API_URL + '/stock/stockGroup');
  }

  getStockGroup(id) {
    return this._http.get(API_URL + '/stock/stockGroup/' + id);
  }

  addStockGroupS(data) {
    return this._http.post(API_URL + '/stock/stockGroup', data);
  }
}
