import { TestBed } from '@angular/core/testing';

import { StockGroupsService } from './stock-groups.service';

describe('StockGroupsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StockGroupsService = TestBed.get(StockGroupsService);
    expect(service).toBeTruthy();
  });
});
