const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const passport = require("passport");
const cors = require('cors');

const app = express();

app.use(cors());

const users = require("./routes/api/users");
const accounts = require("./routes/api/accounts");
const stock = require("./routes/api/stocks");
const company = require("./routes/api/company");

// const stock = require("./routes/api/stock/stock");

//Body Parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//db config
const db = require("./config/keys").mongoURI;

//connect to mongodb
mongoose
  .connect(db, { useNewUrlParser: true })
  .then(() => console.log("mongodb connected"))
  .catch(err => console.log(err));

//Passport middleware
app.use(passport.initialize());

//Passport Config
require("./config/passport")(passport);

//use routes
app.use("/api/company", company);
app.use("/api/users", users);
app.use("/api/account", accounts);
app.use("/api/stock", stock);

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server running on port ${port}`));
